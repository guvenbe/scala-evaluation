package com.kalibri.eval

import com.kalibri.csv.FileReaderWriter
import com.kalibri.spark.SparkManager
import org.apache.spark.sql.DataFrame

class EvalWorker extends SparkManager {

  val fileReaderWriter = new FileReaderWriter

  def readHotels(): DataFrame = {
    
    fileReaderWriter.getDataFromCsvFile("hotels.csv")

  }
  
  def write(dataFrame: DataFrame, directory: String): Unit = {
    fileReaderWriter.writeToCsvFile(dataFrame, directory)
  }
  
}
