package com.kalibri.eval

object EvalRunner {

  def main(args: Array[String]): Unit = {

    println("Started the evaluation")

    val evalWorker = new EvalWorker

    val hotelDf = evalWorker.readHotels()

    // clean the input of duplicates

    // explode the data for missing days (assume date range of 2020-01-01 - 2020-12-31)
    // property 1,123,123,2020-01-01,2020-01-31
    // property 1,150,123,2020-02-01,2020-05-31
    // property 1,150,150,2020-06-01,2020-12-31
    // property 2,120,120,2020-01-01,2020-12-31
    // property 3,120,120,2020-01-01,2020-02-29
    // property 3,120,130,2020-03-01,2020-12-31
    // should become
    // property 1,123,123,2020-01-01
    // property 1,123,123,2020-01-02
    // property 1,123,123,2020-01-03
    // property 1,123,123,2020-01-04
    // property 1,123,123,2020-01-05
    // ...
    // property 1,150,123,2020-02-01
    // property 1,150,123,2020-02-02
    // property 1,150,123,2020-02-03
    // property 1,150,123,2020-02-04
    // ...
    // property 1,150,123,2020-12-29
    // property 1,150,123,2020-12-30
    // property 1,150,123,2020-12-31
    // property 2,120,120,2020-01-01
    // ...
    // property 2,120,120,2020-12-31
    // property 3,120,120,2020-01-01
    // ...
    // property 3,120,130,2020-12-31

    evalWorker.write(hotelDf, "exploded")

    // produce a versioned-by-property-by-date data frame from the original csv
    // example:
    // property name,available rooms,available meeting space,change date,version
    // property 1,123,123,2020-01-01,1
    // property 1,150,123,2020-02-01,2
    // property 1,150,150,2020-06-01,3
    // property 2,120,120,2020-01-01,1
    // property 3,120,120,2020-01-01,1
    // property 3,120,130,2020-03-01,2

    evalWorker.write(hotelDf, "versions")

    System.exit(0)
  }

}
